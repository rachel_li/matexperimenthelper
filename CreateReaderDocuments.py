################################################################
# DT Tran
# Create the documents for the Human Detection Experiment reader binder
################################################################
import sys
import getopt
from HumanDetectionExperiment import *



def main(argv):
    """Instruct the Human Detection Experiment class to create reader documents
    --raw is the directory of the surrogatized documents in raw format
    --json is for using json format rather than raw
    --out is the directory where this will create the reader documents
    --list is a tab-delimited csv file with 3 column headings
        filename->experiment number->experiment doc sequence number (1,2,3...)
    
    """
    try:
        opts, args = getopt.getopt(argv, "r:o:l:h", ["raw=", "out=", "list=", "json=", "help="])
    except getopt.GetoptError:
        sys.exit(2)

    filesLocation = ''
    outDir = ''
    fileList = ''
    is_raw = True
    for opt, arg in opts:
        if opt in ("-r", "--raw"):
            filesLocation = arg
        elif opt in ("-o", "--out"):
            outDir = arg
        elif opt in ("-l", "--list"):
            fileList = arg
        elif opt in ("-h", "--help"):
            print main.__doc__
            sys.exit()
        elif opt in ("-j", "--json"):
            filesLocation = arg
            is_raw = False

    if outDir == '' or filesLocation == '' or fileList == '':
        print "Missing at least one required parameter"
        print main.__doc__
        sys.exit()

    exp = HumanDetectionExperiment(fileList, filesLocation, surrFormat='raw' if is_raw else 'json')
    exp.prepareDir(outDir)
    exp.createReaderDocuments(outDir)

if __name__ == "__main__":
    main(sys.argv[1:])
