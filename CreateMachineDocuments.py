################################################################
# DT Tran
# Create the mat-json documents for the machine detection experiment
# Need to run CreateGoldDetails.py to make a gold_details.csv 
# before this program
################################################################
import sys
import getopt
from HumanDetectionExperiment import *


def main(argv):
    """Instruct the Human Detection Experiment class to create machine detection documents
    --json is the directory of the surrogatized documents in mat-json format
    --gold is the gold_details.csv file created by CreateGoldDetails.py
    --out is the directory where this will create the machine detection documents 
    --list is a tab-delimited csv file with 3 column headings
        filename->experiment number->experiment doc sequence number (1,2,3...)
    
    """
    try:
        opts, args = getopt.getopt(argv, "j:g:o:l:h", ["json=", "gold=", "out=", "list=", "help="])
    except getopt.GetoptError:
        sys.exit(2)

    jsonFilesLocation = ''
    outDir = ''
    fileList = ''
    goldDetailsFile = ''

    for opt, arg in opts:
        if opt in ("-j", "--json"):
            jsonFilesLocation = arg
        elif opt in ("-g", "--gold"):
            goldDetailsFile = arg
        elif opt in ("-o", "--out"):
            outDir = arg
        elif opt in ("-l", "--list"):
            fileList = arg
        elif opt in ("-h", "--help"):
            print main.__doc__
            sys.exit()

    if (outDir == '' or jsonFilesLocation == '' or fileList == '' or goldDetailsFile == ''):
        print "Missing at least one required parameter"
        print main.__doc__
        sys.exit()

    exp = HumanDetectionExperiment(fileList, jsonFilesLocation, surrFormat='mat-json')
    #exp.prepareDir(outDir)
    exp.createMachineDetectionDocuments(goldDetailsFile, outDir)



if __name__ == "__main__":
    main(sys.argv[1:])
