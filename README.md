How to use the files in this zip file:

1. Begin with CreateGoldDetails.py
2. Then you can use its output gold_details.csv file to feed to any of the following tasks if required:
    * CreateReaderDocuments.py
    * CreateGoldDocuments.py
    * CreateMachineDocuments.py

Important: 

* most of these tasks assume that the folders you specified at the command prompt are already there
* please use a new folder to test this code because it will override existing files
* testing: this code has not been thoroughly tested nor streamlined. In my own usage, my files are on a network drive so I usually specify the folder like this 
--source N:\file.txt


# DOCUMENTATION  #
## Create Gold Details ##
CreateGoldDetails.py

Generates a gold_details file combining the details.csv file output by mist with information about surrogates and the form of all these in the "reader" corpus.


```
--details	Details.csv file as output by MIST in an experiment run
--ref	    directory of the original annotated mat-json files; 
	          corresponds to the "test" set specified in xml config file in experiment run
	           <corpus name="test">
	             <pattern>..\data\test\*</pattern>
	           </corpus>
--surr	  directory of the clear to clear surrogatized mat-json files;
	  See "Appendix#Clear to Clear" below
--out	  output directory for gold_details.csv to be created
--hyp	  directory of the experiment engine's output mat-json files;
	  corresponds to ExperimentEngine's runs\test_run\model\hyp
--list	  (optional) list of files to include in gold_details.csv
```


**In running CreateGoldDetails.py, certain annotations may require modification in order to fit into the gold_details scheme. Many of these have been automated.

## Create Reader Documents ##

CreateReaderDocuments.py

Generates HTML documents without markings for review by human annotators.

```
--raw	directory of the surrogatized documents in raw format; see Appendix#Raw
--json	(Not sure if this will work?, I'd use --raw option) for using json format rather than raw
--out	directory for output reader documents
--list	Per the code's documentation:
	"tab-delimited csv file describing which test documents should be included and how they should be included"
	Format:
	filename [\t] experiment number [\t] experiment doc sequence number 
	See example in "Appendix#Documents CSV File"
```
## Create Gold Documents (Grading Sheet) ##
CreateGoldDocuments.py

Generates HTML documents with markings from the gold_details.csv file.

**Same options as CreateReaderDocuments.py, except for the addition of "--gold" option.

```
--raw	directory of the surrogatized documents in raw format;
see Appendex#Raw
--json	(Not sure if this will work?, I'd use --raw option) for using json format rather than raw
--out	directory for output reader documents
--list	Per the code's documentation:
	"tab-delimited csv file describing which test documents should be included and how they should be included"
	Format:
	filename [\t] experiment number [\t] experiment doc sequence number 
	See example in "Appendix#Documents CSV File"
--gold	path to gold_details.csv file created by CreateGoldDetails.py
```
**In running CreateGoldDocuments.py, certain annotations may require modification.

## Combine Reader/Gold Documents ##
The reader documents and gold documents can be combined (independently) to form a single document for the reader docs and a single document for the gold documents (i.e., the grading sheet). This is usually done in a shell script:
	* Append the different html documents together
	* Replace (with "." matching newline):
		* </body>.*?</html>
		* <p style="page-break-after:always">&nbsp;</p></body></html>

## Appendix ##
### Clear to Clear ###
```
%MAT_PKG_HOME%\bin\MATEngine 
--task "HIPS Deidentification" 
--workflow Demo 
--steps nominate,transform 
--replacer "clear -> clear" 
--flag_unparseable_seeds "" 
--input_file_type mat-json 
--input_dir "INPUT_DIR" 
--output_file_type mat-json 
--output_dir "OUTPUT_DIR"
```
### Raw ###
```
%MAT_PKG_HOME%\bin\MATTransducer.cmd 
--input_dir "surrogatized mat-json files from Appendex#Clear to Clear"
--input_file_type mat-json 
--output_file_type raw 
--output_dir "OUTPUT_DIR" 
--output_encoding 'windows-1252'  # may vary depending on your OS (?)
```

### Documents CSV File ###
```
DocNum	Exp	Row
3489	5	1
4873	5	2
4884	5	3
1125	5	4
4826	5	5
4842	5	6
...	...	...
4868	5	100
```

### Mat-json Compare ###
A Python file to compare two mat-json files with the intention of checking for differences in indices.


# LICENSE #
Licensed under MIT License.

The MIT License (MIT)

Copyright (c) 2015 Group Health Research Institute

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.