################################################################
# DT Tran
# Create the html documents for the human detection experiment binder
# Need to run CreateGoldDetails.py to make a gold_details.csv 
# before this program
################################################################
import sys
import getopt
from HumanDetectionExperiment import *


def main(argv):
    """Instruct the Human Detection Experiment class to create reader documents
    --raw is the directory of the surrogatized documents in raw format
    --json is for using json format rather than raw
    --gold is the gold_details.csv file created by CreateGoldDetails.py
    --out is the directory where this will create the grading sheets 
    --list is a tab-delimited csv file with 3 column headings
        filename->experiment number->experiment doc sequence number (1,2,3...)
    
    """
    try:
        opts, args = getopt.getopt(argv, "r:g:o:l:h", ["raw=", "json=", "gold=", "out=", "list=", "help="])
    except getopt.GetoptError:
        sys.exit(2)

    filesLocation = ''
    outDir = ''
    fileList = ''
    is_raw = True
    goldDetailsFile = ''

    for opt, arg in opts:
        if opt in ("-r", "--raw"):
            filesLocation = arg
        elif opt in ("-g", "--gold"):
            goldDetailsFile = arg
        elif opt in ("-o", "--out"):
            outDir = arg
        elif opt in ("-l", "--list"):
            fileList = arg
        elif opt in ("-h", "--help"):
            print main.__doc__
            sys.exit()
        elif opt in ("-j", "--json"):
            filesLocation = arg
            is_raw = False

    if outDir == '' or filesLocation == '' or fileList == '' or goldDetailsFile == '':
        print "Missing at least one required parameter"
        print main.__doc__
        sys.exit()

    exp = HumanDetectionExperiment(fileList, filesLocation, surrFormat='raw' if is_raw else 'json')
    exp.prepareDir(outDir)
    exp.createGradingSheets(goldDetailsFile, outDir)



if __name__ == "__main__":
    main(sys.argv[1:])
