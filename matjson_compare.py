"""
Compare the indices of two matjson files, and print/output the differences.
"""
import argparse
import ast


def read(f):
    with open(f) as fh:
        return ast.literal_eval(fh.read().replace('true', 'True').replace('false', 'False').replace('null', 'None'))


def compare(x, y, title):
    print '{}:\t'.format(title),
    if x == y:
        print 'EQUAL'
    else:
        print 'NOT EQUAL'


def sort(asets):
    return sorted(asets, key=lambda x: x['type'])


def cmp_annots(annot1, annot2, atype):
    print '************************'
    print '**TYPE: {}'.format(atype)
    i = 0
    offset1 = 0
    offset2 = 0
    while i < max(len(annot1), len(annot2)):
        if i < len(annot1) and i < (len(annot2)):
            val_1a, val_1b = annot1[i+offset1][0:2]
            val_2a, val_2b = annot2[i+offset2][0:2]
            if val_1a == val_2a and val_1b == val_2b:
                pass  # everything looks good
            elif val_1a == val_2a:
                print 'MISTMATCH END: {}, {}; {}, {}'.format(val_1a, val_1b, val_2a, val_2b)
            elif val_1b == val_2b:
                print 'MISTMATCH START: {}, {}; {}, {}'.format(val_1a, val_1b, val_2a, val_2b)
            else:
                if val_1a < val_2a and val_1b < val_2b:
                    offset1 += 1
                    print 'OVERLAP 1<2: {}, {}; {}, {}'.format(val_1a, val_1b, val_2a, val_2b)
                elif val_1a > val_2a and val_1b > val_2b:
                    offset2 += 1
                    print 'OVERLAP 1>2: {}, {}; {}, {}'.format(val_1a, val_1b, val_2a, val_2b)
                elif val_1a < val_2a and val_1b > val_2b:
                    offset2 += 1
                    print 'OVERWHELM: {}, {}; {}, {}'.format(val_1a, val_1b, val_2a, val_2b)
                elif val_1a > val_2a and val_1b < val_2b:
                    offset2 += 1
                    print 'UNDERWHELM: {}, {}; {}, {}'.format(val_1a, val_1b, val_2a, val_2b)
                else:
                    raise ValueError('Overlap went wrong: {}, {}; {}, {}'.format(val_1a, val_1b, val_2a, val_2b))
        elif i < len(annot1):
            print "EXTRA ANNOTATION: {} -".format(annot1[i+offset1])
        elif i < len(annot2):
            print "EXTRA ANNOTATION: -  {}".format(annot2[i+offset2])
        else:
            print 'UNKNOWN ISSUE: ELSE LOOP ENCOUNTERED, LOOP #{} ({}-{})'.format(i, offset1, offset2)
        i += 1
        if offset1 > 0 and offset2 > 0:
            i += min(offset1, offset2)
            offset1 -= min(offset1, offset2)
            offset2 -= min(offset1, offset2)


def cmp_asets(asets1, asets2):
    print 'ANNOTATION COMPARE:'
    offset = 0
    for i in range(len(asets1)):
        while len(asets2) < i+offset and asets1[i]['type'] != asets2[i+offset]['type']:
            offset += 1
        if len(asets2) < i+offset:
            break
            
        cmp_annots(asets1[i]['annots'], asets2[i+offset]['annots'], asets1[i]['type'])
            

def main(file1, file2):
    d1 = read(file1)
    d2 = read(file2)

    compare(d1['signal'], d2['signal'], 'SIGNAL/TEXT')
    compare(d1['metadata'], d2['metadata'], 'METADATA')
    compare(d1['version'], d2['version'], 'VERSION')

    cmp_asets(sort(d1['asets']), sort(d2['asets']))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
    parser.add_argument('file1',
                        help='A matjson file.')
    parser.add_argument('file2',
                        help='A matjson file.')

    args = parser.parse_args()

    main(**vars(args))