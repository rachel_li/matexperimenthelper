################################################################
# DT Tran
################################################################

import string
import difflib
import shutil
import errno

from MATExperimentDetail import *
from MatJSONDocument import *


class HumanDetectionExperiment():
    """Human detection experiment to test MIST's ability to hide PII in plain sight"""

    ############## global ############################################################
    # I got these colors from the MIST's task.xml file,this task file was used in the 
    # MIST workspace to create the gold standard for these documents
    piiColors = {
        'org_name': '#FFCC66',
        'pt_name': '#CCFF66',
        'doc_name': '#FF99CC',
        'date': '#99CCFF',
        'other_id': '#FFFF33',
        'address': '#00CC99',
        'phone': '#66FFCC',
        'age': '#99CC66',
        'email': '#336633',
        'ip_addr': '#996633',
        'url': '#FFCC00',
        'room': '#CC00FF',
        'doc_initials': '#CC9900',
        'med_rec_num': '#FF3300',
        'ssn': '#006600'
    }

    t = string.Template("""
    <!DOCTYPE html>
    <html><head><title>$documentid</title>
    <style type="text/css">
        body{width:100%;margin:0;float:none;padding:0;}
        .pii {text-decoration:underline;}
        .leak {border:2px solid Black;font-weight:bold;}
        .irb {border:2px dashed Black;font-weight:bold;}
        .org_name{ background-color: $org_name;text-decoration:underline;}
        .pt_name{ background-color: $pt_name; text-decoration:underline;}
        .doc_name{ background-color: $doc_name;text-decoration:underline;}
        .date{ background-color: $date;text-decoration:underline;}
        .other_id{ background-color: $other_id;text-decoration:underline;}
        .address{ background-color: $address;text-decoration:underline;}
        .phone{ background-color: $phone;text-decoration:underline;}
        .age{ background-color: $age;text-decoration:underline;}
        .email{ background-color: $email; color: white;text-decoration:underline;}
        .ip_addr{background-color: $ip_addr; color: white;text-decoration:underline;}
        .url{ background-color: $url;text-decoration:underline;}
        .room{ background-color: $room; color: white;text-decoration:underline;}
        .doc_initials{background-color: $doc_initials; color: white;text-decoration:underline;}
        .med_rec_num{ background-color: $med_rec_num; color: white;text-decoration:underline;}
        .ssn{background-color: $ssn; color: white;text-decoration:underline;}
        @media print{
        div.divFooter{position:fixed;bottom:0;}
        div.divHeader{position:fixed;top:0px;height:100px;}
        }
        @media screen{
            div.divFooter{}
            div.divHeader{}
        }
   
        @frame footer {
        -pdf-frame-content: footerContent;
        bottom: 2cm;
        margin-left: 1cm;
        margin-right: 1cm;
        height: 1cm;
        }

    
    </style>
    </head>
    <body style="font-family:Verdana;font-size:13.5pt;">

    <div>
    $filename | Experiment:$experimentid | Document Sequence#:$documentid <br /><br />
    NOTICE: PERSONAL IDENTIFYING INFORMATION IN THIS DOCUMENT, SUCH AS NAMES AND DATES, HAVE BEEN REPLACED WITH FICTIONAL REPLACEMENTS THAT APPEAR TO BE REAL BUT DO NOT REFER TO ANY ACTUAL PEOPLE OR EVENTS. THIS IS DONE TO PROTECT THE IDENTITY OF PERSONS REFERENCED IN THE DOCUMENT. RESEARCH USE OF THIS DOCUMENT IS STRICTLY GOVERNED BY A DATA USE AGREEMENT.<br /><br />
    <div style="line-height:2em; margin-right:150px;font-size:12pt;text-align:justify;">
    $documenttext
    </div>
    <div>

    </body>
    </html>
    """)
    matjsonTemplate = string.Template(
        """{"signal": $signal, "asets": $asets, "version": $version, "metadata": $metadata}""")

    types = ['org_name', 'pt_name', 'doc_name', 'date', 'other_id'
        , 'address', 'phone', 'age', 'email', 'ip_addr', 'url'
        , 'med_rec_num', 'ssn', 'room', 'doc_initials']

    def __init__(self, fileList, surrDirectory, fileExt='.prepped.tag.json', surrFormat='raw'):
        """Initialize what this Human Detection Experiment needs to know"""

        self.fileList = fileList
        self.surrDirectory = surrDirectory
        self.fileExt = fileExt
        self.surrogateFiles = self.getSurrogateFiles(fileExt, surrFormat)

    def getGoldDetails(self, goldDetailsFile):
        """Return the contents of the gold_details.csv file as a list of MATExperimentDetail"""

        details = []
        with open(goldDetailsFile, 'rb') as f:
            reader = csv.reader(f)
            rowNumber = 0
            for row in reader:
                rowNumber += 1
                if rowNumber > 1:
                    details.append(MATExperimentDetail(row[0], row[1], row[2], row[3], row[4],
                                                       row[5], row[6], row[7], row[8], row[9],
                                                       row[10], row[11], row[12], row[13],
                                                       row[14], row[15], row[16], row[17], row[18]))
        return details

    def getSurrogateFiles(self, fileExt, surrFormat):
        """ Return a dictionary where the file name is the key. 
        The dictionary stores document_text, document_name, experiment_number, experiment_doc_number

        fileList is a tab-delimited csv file with 3 column headings
        filename->experiment number->experiment doc sequence number (1,2,3...)
    
        fileDirectory is where to read in the files listed in column 1 of fileList
        fileExt is the file extension for the files listed in column 1 of fileList
    
        """
        fileListRows = []
        surrogateFiles = {}
        with open(self.fileList, 'rb') as csvfile:
            inputreader = csv.reader(csvfile, delimiter='\t')
            for row in inputreader:
                if not row[0].lstrip().startswith('#'):  # ignore rows starting with '#'
                    fileListRows.append(row)

        # skip the first row for row header
        for f in fileListRows[1:]:
            filename, experiment, docnumber = f
            if surrFormat == 'raw':
                text = open(self.surrDirectory + '\\' + filename, 'rb').read()
                if filename not in surrogateFiles:
                    fileRoot = filename[0:str.find(filename, fileExt)]
                    surrogateFiles[filename] = {'document_text': text
                        , 'document_name': fileRoot
                        , 'experiment_number': experiment
                        , 'experiment_doc_number': str(docnumber).zfill(4)
                                                }
            else:
                doc = MatJSONDocument(self.surrDirectory + '\\' + filename)
                text = doc.text

                if filename not in surrogateFiles:
                    fileRoot = filename[0:str.find(filename, fileExt)]
                    surrogateFiles[filename] = {'document_text': text
                        , 'document_name': fileRoot
                        , 'experiment_number': experiment
                        , 'experiment_doc_number': str(docnumber).zfill(4)
                        , 'version': doc.version
                        , 'metadata': doc.metadata
                        , 'pii_annots': doc.getAsetType(self.types)
                        , 'other': doc.getAsetType(['lex', 'zone', 'SEGMENT'])
                                                }
                else:
                    raise

        return surrogateFiles

    def createGradingSheets(self, goldDetailsFile, outDirectory):
        """Create the html with styles to describe leak and surrogate types"""
        goldDetails = self.getGoldDetails(goldDetailsFile)
        if not goldDetails:
            raise ValueError('Unable to locate any documents in gold details file.')
        prevFileName = None
        for row, detail in enumerate(goldDetails):
            filename = detail.filename.split('.')[0]
            if filename not in self.surrogateFiles:
                print "Skipping: {0}".format(filename)
                continue
            # if new file, but not last row in details.csv, write out previous file now
            if filename != prevFileName:
                # deal with previous record
                if prevFileName:
                    self.output_html_file(expRecord, prevFileName, outDirectory, start=start,
                                          split_content=splitContent)
                    # to remove out of experimentInfo so I know which file is not in details.csv
                    self.surrogateFiles.pop(prevFileName)

                # setup context for current record
                expRecord = self.surrogateFiles[filename]
                content = expRecord.get('document_text')
                splitContent = []
                start = 0

            if detail.type == "missing":
                piiMarkup = '<span class="' + detail.goldlabel + ' leak">'
            elif detail.type == "spurious":  # adding 6/12
                piiMarkup = '<span style="border-bottom: 2px dashed ' + self.piiColors[detail.goldlabel] + ';">'
            else:
                # note that word doc doesn't seem to recognize multiple classes this way
                piiMarkup = '<span class="' + detail.goldlabel + '">'

            if detail.isOvermark() or detail.isOverlap():  # add overlap condition 6/12
                frleak = detail.getFrontLeakOffsets()
                tleak = detail.getTailLeakOffsets()
                if frleak or tleak:
                    leakMarkup = '<span class="' + detail.reflabel + ' irb">'
                    if frleak and tleak:
                        splitContent.append(content[start:frleak[0]])
                        splitContent.append(leakMarkup + content[frleak[0]:frleak[1]] + '</span>')
                        splitContent.append(piiMarkup + content[detail.get_surstart():detail.get_surend()] + '</span>')
                        splitContent.append(leakMarkup + content[tleak[0]:tleak[1]] + '</span>')
                    elif frleak:
                        splitContent.append(content[start:frleak[0]])
                        splitContent.append(leakMarkup + content[frleak[0]:frleak[1]] + '</span>')
                        splitContent.append(piiMarkup + content[detail.get_surstart():detail.get_surend()] + '</span>')
                    elif tleak:
                        splitContent.append(content[start:detail.get_surstart()])
                        splitContent.append(piiMarkup + content[detail.get_surstart():detail.get_surend()] + '</span>')
                        splitContent.append(leakMarkup + content[tleak[0]:tleak[1]] + '</span>')

            else:
                splitContent.append(content[start:detail.get_goldstart()])
                splitContent.append(piiMarkup + content[detail.get_goldstart():detail.get_goldend()] + '</span>')

            # move the start and update the filename
            start = detail.get_goldend()
            prevFileName = filename

        # final fencepost
        try:
            self.output_html_file(expRecord, prevFileName, outDirectory, start=start, split_content=splitContent)
            self.surrogateFiles.pop(prevFileName)
        except UnboundLocalError:
            raise ValueError('No records found.')

        # print left over files that were not in the gold_details.csv as normal reader documents
        self.createReaderDocuments(outDirectory)

    def output_html_file(self, exp_record, filename, outdir, start=None, split_content=None):
        if start:
            split_content.append(exp_record['document_text'][start:])
            text = ''.join(split_content)
            if not self.truthTest(text, exp_record['document_text']):
                raise ValueError('Document text does not match: {}.'.format(filename))
        else:
            text = exp_record['document_text']

        text = re.sub('\r\n', '<br />', text)
        self.writeToFile(os.path.join(outdir, exp_record['experiment_doc_number'] + '.html'),
                         self.fillTemplate(exp_record['experiment_number'],
                                           text,
                                           exp_record['experiment_doc_number'],
                                           filename))

    def createReaderDocuments(self, outDirectory):
        """Create the reader documents with some format but without PII mark up to outDirectory"""
        for filename in self.surrogateFiles:
            self.output_html_file(self.surrogateFiles[filename], filename, outDirectory)

    def fillTemplate(self, experimentid, documenttext, documentid, filename):
        """Substitute the place holders in the template with real values and return a new string"""

        values = {'experimentid': experimentid,
                  'documenttext': documenttext,
                  'filename': filename.replace('.prepped.tag.json', ''),
                  'documentid': documentid,
                  'org_name': self.piiColors['org_name'],
                  'pt_name': self.piiColors['pt_name'],
                  'doc_name': self.piiColors['doc_name'],
                  'date': self.piiColors['date'],
                  'other_id': self.piiColors['other_id'],
                  'address': self.piiColors['address'],
                  'phone': self.piiColors['phone'],
                  'age': self.piiColors['age'],
                  'email': self.piiColors['email'],
                  'ip_addr': self.piiColors['ip_addr'],
                  'url': self.piiColors['url'],
                  'room': self.piiColors['room'],
                  'doc_initials': self.piiColors['doc_initials'],
                  'med_rec_num': self.piiColors['med_rec_num'],
                  'ssn': self.piiColors['ssn']
                  }
        return self.t.substitute(values)

    def fillMatJsonTemplate(self, documenttext, pii_annots, version, metadata, other):
        """Substitute the place holders in the template with real values and return a new matjson string"""
        asets = []
        pii_annots.update(other)
        for type in pii_annots:
            aset = {}
            if type == 'SEGMENT':
                aset = collections.OrderedDict([("hasID", False), ("type", type)
                                                   ,
                                                ("attrs", [{"type": "string", "name": "annotator", "aggregation": None}
                                                    , {"type": "string", "name": "status", "aggregation": None}
                                                    , {"type": "string", "name": "to_review", "aggregation": None}
                                                    , {"type": "string", "name": "reviewed_by", "aggregation": None}])
                                                   , ("annots", other[type]), ("hasSpan", True)])
            elif type == 'zone':
                aset = collections.OrderedDict([("hasID", False), ("type", type)
                                                   , ("attrs",
                                                      [{"type": "string", "name": "region_type", "aggregation": None}])
                                                   , ("annots", other[type]), ("hasSpan", True)])
            else:
                aset = collections.OrderedDict(
                    [("hasID", False), ("type", type), ("attrs", []), ("annots", pii_annots[type]), ("hasSpan", True)])

            asets.append(aset)


        # thought I have to mimic MIST, put backlash infront of quote in text and before line return
        # but this doesn't apply in this program the way documenttext is read in and write out
        # documenttext = re.sub('"', r'\\"', documenttext)
        # documenttext = re.sub('\r\n', r'\\r\\n', documenttext)

        doc = collections.OrderedDict(
            [("signal", documenttext), ("asets", asets), ("version", version), ("metadata", metadata)])
        return json.dumps(doc)

    def writeToFile(self, filepath, content):
        """Write content to this filepath"""
        with open(filepath, 'wb') as fOut:
            fOut.write(content)

    def prepareDir(self, directory, ignore_error=True):
        """Create the directory if doesn't exist, else raise error"""
        try:
            os.makedirs(directory)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(directory):
                pass
            elif ignore_error:
                pass
            else:
                raise

    def truthTest(self, htmlDoc, rawDoc):
        html = re.compile('</span>', re.UNICODE)
        stripText = html.sub('', htmlDoc)
        html = re.compile('<span .*?>', re.UNICODE)
        stripText = html.sub('', stripText)

        if ' '.join(stripText.split()) != ' '.join(rawDoc.split()):
            print "Differences:"
            print '\n'.join(
                [x for x in (difflib.Differ().compare(stripText.split('.'), rawDoc.split('.'))) if x[0] != ' '])
            return False
        return True

    def createMachineDetectionDocuments(self, goldDetailsFile, outDirectory):
        """Create the mat-json files with combined leaks and surrogates annotations
        This does not change the document text, it just compile the asets, metadata, version info"""
        goldDetails = []
        goldDetails = self.getGoldDetails(goldDetailsFile)
        prevFileName = ''
        numrow = len(goldDetails)

        goldDetailsIter = iter(goldDetails)
        detail = next(goldDetailsIter, None)
        while detail is not None:
            nextDetail = next(goldDetailsIter, None)
            filename = detail.filename
            if filename not in self.surrogateFiles:
                # if this file was not on the surrogate file list
                # update the filename and move to the next row
                prevFileName = filename
                detail = nextDetail
                continue
            if filename != prevFileName:
                # if it's a new file,
                # reset some variables
                pii_annots = {}

            # add offsets of current detail row to pii_annots
            if detail.goldlabel in pii_annots:
                pii_annots[detail.goldlabel].append([int(detail.goldstart), int(detail.goldend)])
            else:
                pii_annots[detail.goldlabel] = [[int(detail.goldstart), int(detail.goldend)]]

            if nextDetail is None or filename != nextDetail.filename:
                # if it's the last row in gold details or it's the last row of pii for the mat-json file
                expRecord = self.surrogateFiles[filename]
                documenttext = expRecord.get('document_text')
                version = expRecord.get('version')
                metadata = expRecord.get('metadata')
                other = expRecord.get('other')
                # remove file from surrogateFiles list so I know which files are not in the gold details file
                self.surrogateFiles.pop(filename, None)
                filledTemplate = self.fillMatJsonTemplate(documenttext, pii_annots, version, metadata, other)
                filepath = os.path.join(outDirectory, filename + '.machine')
                self.writeToFile(filepath, filledTemplate)

            # update the filename and move to the next row
            prevFileName = filename
            detail = nextDetail

        # copy all remaining mat-json files from surrogate directory to out as is
        # these do not have known PII so they are not in the gold details file
        self.copyMatJsonFiles(outDirectory)

    def copyMatJsonFiles(self, out):
        for filename in self.surrogateFiles:
            shutil.copy(os.path.join(self.surrDirectory, filename), out)
