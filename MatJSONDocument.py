############################################################################
# DT Tran
# There is probably already something written in the MIST install, 
# but to simplify, i'll just write something here 
############################################################################

import json
import os
import re
import collections

class MatJSONDocument():
    """a Mat-json document with the basic components we have used"""

    def __init__(self, filePath):
        with open(filePath, 'rb') as fileIn:
            data = fileIn.read()
        fileIn.closed
        
        self.text = ''
        self.asets = {}
        try:
            self.contents = json.loads(data)
            self.version = self.contents['version']
            self.metadata = self.contents['metadata']
            self.text = self.getText()
            self.asets = self.getAsets()
        except Exception as e:
            print filePath + 'could not load json data'
            raise
    
    def getText(self):
        """returns the original signal text of the document""" 

        text = self.contents['signal']
        text = text.encode('utf-8') #commented out 1/31/2014 due to incorrect offsets for Temperature
        

        #MatJSON file can contain a backlash \ in front of double quote " 
        #when the quote is somewhere in the middle of the text to preserve the quote
        #This can cause problem with using the aset offset to get the correct text span, 
        #don't include it, same with \r\n
        text = re.sub(r'\\"', '"', text)
        text = re.sub(r'\\r\\n', '\r\n', text)

        return text

    def getAsets(self):
        """returns a dictionary of all aset types in this file
        key: aset type, value: list of aset offsets"""

        asetsDictionary = {}
        asets = self.contents['asets']
        
        for index in range(len(asets)):
            anot = asets[index]
            asetsDictionary[anot['type']] = anot['annots']

        return asetsDictionary
        

    def getAsetType(self, types):
        """returns a dictionary of specific aset types if it exists in the file """

        asets = {}
        #TODO when types is only 1 type and not a list
        for i in range(len(types)):
            type = types[i]
            if type in self.asets:
                asets[type] = self.asets[type]

        return asets
    
    def getAsetTypeCount(self, types):
        """returns a dictionary of specific aset types and count of instances per type"""
        
        asetsCount = {}
        asets = self.getAsetType(types)
        for type in types:
            if type in asets:
                asetsCount[type] = len(asets[type])
            else:
                asetsCount[type] = 0

        return asetsCount

    def getAsetTypeList(self, types):
        """returns a list of asets, each item has type, begin, end"""
        returnList = []

        dictionary = self.getAsetType(types)
        for t in types:
            if t in dictionary:
                annots = dictionary[t]
                for annot in annots:
                    returnList.append({"type":t, "begin": annot[0], "end": annot[1]})
        return returnList

    def fillMatJsonTemplate(self, documenttext, pii_annots, version, metadata, other):
        """Substitute the place holders in the template with real values and return a new matjson string"""
        asets = []
        pii_annots.update(other)
        for type in pii_annots:
            aset = {}
            if type =='SEGMENT':
                aset = collections.OrderedDict([("hasID", False), ("type", type)
                                                , ("attrs", [{"type": "string", "name": "annotator", "aggregation": None}
                                                    , {"type": "string", "name": "status", "aggregation": None}
                                                    , {"type": "string", "name": "to_review", "aggregation": None}
                                                    , {"type": "string", "name": "reviewed_by", "aggregation": None}])
                                                , ("annots", other[type]), ("hasSpan", True)])
            elif type == 'zone':
                aset = collections.OrderedDict([("hasID", False), ("type", type)
                                                , ("attrs", [{"type": "string", "name": "region_type", "aggregation": None}])
                                                , ("annots", other[type]), ("hasSpan", True)])
            else:
                aset = collections.OrderedDict([("hasID", False), ("type", type), ("attrs", []), ("annots", pii_annots[type]), ("hasSpan", True)])
            
            asets.append(aset)


        #thought I have to mimic MIST, put backlash infront of quote in text and before line return
        #but this doesn't apply in this program the way documenttext is read in and write out
        #documenttext = re.sub('"', r'\\"', documenttext)
        #documenttext = re.sub('\r\n', r'\\r\\n', documenttext)
   
        doc = collections.OrderedDict([("signal", documenttext), ("asets", asets), ("version", version), ("metadata", metadata)])
        return json.dumps(doc)

    def makeAsetListToDictionary(self, asetList):
        """returns a dictionary version of the piiList, key is the pii type"""
        asets = {}
        for p in asetList:
            if p['type'] in asets:
                asets[p['type']].append([p['begin'], p['end']])
            else:
                asets[p['type']] = [[p['begin'], p['end']]]
        
        return asets