################################################################
# DT Tran
# MATExperimentDetail: Created to hold information for each row in gold_details.csv
# MATExperimentGoldDetailsManager: to create custom gold_details.csv
# because the details.csv that MIST produces sometimes splits spanclashes as overmark + spurious or undermark + missing
# This version addresses the basic span clashes the way we like, but occasionally 
# the span clashes can be very complicated and still needs to be reviewed.
################################################################
import sys
from Annotation import *
from MatJSONReader import *


class MATExperimentDetail():
    """a row on the MATExperimentEngine gold_details.csv file with important elements"""

    """ I can force the offset to be 0 if the cell is blank, 
    but then it looks like hypstart is 0 when it's blank so I choose not to store it as an int"""

    def __init__(self, filename, ptype='', reflabel='', hyplabel='', refstart='', refend='',
                 hypstart='', hypend='', refcontent='', hypcontent='',
                 surlabel='', surstart='', surend='', surcontent='',
                 goldlabel='', goldstart='', goldend='', goldcontent='', offsetChange=''):
        self.filename = filename
        self.type = ptype
        self.reflabel = reflabel
        self.hyplabel = hyplabel
        self.refstart = refstart
        self.refend = refend
        self.hypstart = hypstart
        self.hypend = hypend
        self.refcontent = refcontent
        self.hypcontent = hypcontent

        self.surlabel = surlabel
        self.surstart = surstart
        self.surend = surend
        self.surcontent = surcontent

        self.goldlabel = goldlabel
        self.goldstart = goldstart
        self.goldend = goldend
        self.goldcontent = goldcontent
        self.offsetChange = offsetChange

        try:
            self.goldend_i_ = int(self.goldend)
            self.goldstart_i_ = int(self.goldstart)
        except ValueError:
            self.goldend_i_ = None
            self.goldstart_i_ = None
        try:
            self.surend_i_ = int(self.surend)
            self.surstart_i_ = int(self.surstart)
        except ValueError:
            self.surend_i_ = None
            self.surstart_i_ = None

    def has_type(self, ptype):
        return self.type.find(ptype) >= 0

    def get_surstart(self):
        return self.surstart_i_

    def get_goldstart(self):
        return self.goldstart_i_

    def get_goldend(self):
        return self.goldend_i_

    def get_surend(self):
        return self.surend_i_

    def getFrontLeakOffsets(self):
        """Return the offsets of a partial leak that occurs at the beginning of a PII"""
        if self.get_surstart() > self.get_goldstart():
            return self.get_goldstart(), self.get_surstart()
        return None

    def getTailLeakOffsets(self):
        """Return the offsets of a partial leak that occurs at the end of a PII"""
        if self.get_surend() < self.get_goldend():
            return self.get_surend(), self.get_goldend()
        return None

    def isOvermark(self):
        return self.has_type('overmark')

    def isUndermark(self):
        return self.has_type('undermark')

    def isOverlap(self):
        return self.has_type('overlap')

    def isTagclash(self):
        return self.has_type('tagclash')

    def isMatch(self):
        return self.type == 'match'

    def isSpurious(self):
        return self.type == 'spurious'

    def isMissing(self):
        return self.type == 'missing'

    def setDetail(self, ref, hyp, surr, gold, ptype='', offset=0):
        if ref:
            self.reflabel = ref.label
            self.refstart = str(ref.start)
            self.refend = str(ref.end)
            self.refcontent = ref.content
        else:
            self.reflabel = ''
            self.refstart = ''
            self.refend = ''
            self.refcontent = ''

        if hyp:
            self.hyplabel = hyp.label
            self.hypstart = str(hyp.start)
            self.hypend = str(hyp.end)
            self.hypcontent = hyp.content
        else:
            self.hyplabel = ''
            self.hypstart = ''
            self.hypend = ''
            self.hypcontent = ''

        if surr:
            self.surlabel = surr.label
            self.surstart = str(surr.start)
            self.surend = str(surr.end)
            self.surcontent = surr.content
        else:
            self.surlabel = ''
            self.surstart = ''
            self.surend = ''
            self.surcontent = ''

        if gold:
            self.goldlabel = gold.label
            self.goldstart = str(gold.start)
            self.goldend = str(gold.end)
            self.goldcontent = gold.content
        else:
            self.goldlabel = ''
            self.goldstart = ''
            self.goldend = ''
            self.goldcontent = ''

        self.type = ptype
        self.offsetChange = offset


class MATExperimentGoldDetailsManager():
    """manage a list of MATExperimentDetails and compile a list of gold details"""


    def __init__(self, filename, details, originalTokens, references, hypotheticals, surrogates, surrogateFileText):
        self.filename = filename
        self.details = details
        self.originalTokens = originalTokens
        self.references = references
        self.hypotheticals = hypotheticals
        self.surrogates = surrogates
        self.surrogateFileText = surrogateFileText
        self.goldDetails = []
        self.cumulativeOffsetChange = 0

    def process(self):
        """the main method to crunch through a list of experiment details for this file 
            and returns a list of gold details
        """
        if len(self.hypotheticals) != len(self.surrogates):
            # Not always an error, example OR03FP00401D00555.prepped.tag.json overmark, then spurious at the end of the overmark span
            errMsg = "Warning: surrogates has " + str(len(self.surrogates)) + " for " + str(
                len(self.hypotheticals)) + " MIST annotations in " + self.filename
            print errMsg

        while len(self.references) > 0:
            if len(self.hypotheticals) > 0:
                ref = self.references[0]
                hyp = self.hypotheticals[0]
                if ref.end < hyp.start:
                    self.processFullLeak()
                else:
                    self.processNonFullLeak()
            else:
                # len(self.hypotheticals) is 0 but there are still references
                # they are all missing/leaks that MIST didn't get a hypothetical hit
                self.processFullLeak()

        if len(self.references) > 0: raise Exception("did not account for all leaks")

        # in case there are more hypothetical left over after all references are considered
        # or may be there was 0 references
        while len(self.hypotheticals) > 0:
            # they are all spurious that wasn't in original gold
            self.processSpurious()

        if len(self.hypotheticals) > 0: raise Exception("did not account for all spurious pii")
        if False and len(self.surrogates) > 0: raise Exception("did not account for all surrogates")

    def processFullLeak(self):
        """helper method to process(), use to create a 'missing' experiment detail
        in a full leak, the reference is not match by a hypothetical, so there is no surrogate
        and the adjudicated gold is just the reference. Remove the leak instance from references
        """

        if len(self.references) > 0:
            detail = MATExperimentDetail(self.filename)

            ref = self.references.pop(0)
            hyp = None
            surr = None
            gold = Annotation()
            ref.copyTo(gold)
            gold.start += self.cumulativeOffsetChange
            gold.end = gold.start + gold.length
            resultType = 'missing'
            self.addToGoldDetails(detail, ref, hyp, surr, gold, resultType)  # does not need surOverlapLen
        else:
            raise Exception('Expecting to process a full leak but there is no leak item to process in ' + self.filename)

    def processFullSpanMatch(self):
        """helper method to process(), create a 'match' or 'tagclash' experiment detail
        in a full span match, the reference and the hypothetical offsets are the same
        but the label could be different for a tagclash. Remove the ref and hyp pair from their lists"""

        if len(self.references) > 0 and len(self.hypotheticals) > 0:
            ref = self.references[0]
            hyp = self.hypotheticals[0]
            # double check that this method is appropriate for this pair
            print ref.content, hyp.content
            if True or (ref.hasSameContent(hyp)):
                detail = MATExperimentDetail(self.filename)
                self.references.pop(0)
                self.hypotheticals.pop(0)
                surr = self.surrogates.pop(0)
                gold = surr

                if ref.label != hyp.label:
                    resultType = 'tagclash'
                else:
                    resultType = 'match'
                self.addToGoldDetails(detail, ref, hyp, surr, gold, resultType)

            else:
                raise Exception('Expecting a full span match')
        else:
            raise Exception(
                'Expecting to process a match or tagclash but there is no ref and hyp left in ' + self.filename)

    def processSpurious(self):
        """helper method to process(), create a 'spurious' experiment detail,
        MIST annotated a hypothetical instance not in the ref, so MIST surrogatized it. 
        Remove the hyp and surr from their lists
        """
        if len(self.hypotheticals) > 0 and len(self.surrogates) > 0:
            detail = MATExperimentDetail(self.filename)
            ref = None
            hyp = self.hypotheticals.pop(0)
            surr = self.surrogates.pop(0)
            gold = surr
            resultType = 'spurious'
            self.addToGoldDetails(detail, ref, hyp, surr, gold, resultType)
        else:
            raise Exception(
                'Expecting to process a spurious row but there is no ref and surrogate left in ' + self.filename)

    def processOvermarks(self):
        """helper method to process(), keep a single as overmark or create match/tag clash
        , or combine overmark with spurious or keep them separate
        """
        if len(self.references) == 0 or len(self.hypotheticals) == 0:
            raise Exception(
                'Expecting to process an overmark but there is no valid ref or hyp left in ' + self.filename)

        # remove current hyp and see if current ref span also covers the next hyp
        hyp = self.hypotheticals[0]
        ref = self.references.pop(0)

        if not ref.isOvermark(hyp):
            raise Exception(
                'Expecting to process an overmark but ref and hyp is not an overmark pair in ' + self.filename)

        overmarks = ref.findNextOverlap(self.hypotheticals)
        while len(overmarks) > 0:
            # resets
            hyp = self.hypotheticals[0]
            extra = ''
            hypEndToNextHyp = 0
            leakLen = 0
            realLeak = False

            if len(overmarks) == 1:
                # single overmark
                if ((ref.start < hyp.start and ref.end == hyp.end)
                    or (ref.start == hyp.start and ref.end > hyp.end)):
                    surr = self.getSurrogates()
                    detail = MATExperimentDetail(self.filename)
                    # gold needs to contain the leaked part
                    gold = Annotation()
                    surr.copyTo(gold)
                    if ref.start < hyp.start:
                        leakLen = hyp.start - ref.start
                        gold.start -= leakLen
                        gold.content = ref.content[:leakLen] + gold.content
                    else:
                        print gold.content
                        print ref.content
                        leakLen = ref.end - hyp.end
                        gold.end += leakLen
                        gold.content += ref.content[-leakLen:]
                        print gold.content

                    resultType = 'overmark'
                    if ref.label != hyp.label: resultType += ',tagclash'
                    self.addToGoldDetails(detail, ref, hyp, surr, gold, resultType)
                    self.hypotheticals.pop(0)
                elif ref.start == hyp.start and ref.end == hyp.end:
                    self.references.insert(0, ref)
                    self.processFullSpanMatch()
                else:
                    raise Exception('Unexpected case in ' + self.filename)
            else:
                if ref.start != hyp.start:
                    if False:  # todo
                        raise Exception(
                            'Unexpected gap in ' + self.filename + ' ' + str(hyp.start) + ' ' + str(ref.start))
                nextHyp = self.hypotheticals[1]
                hypEndToNextHyp = nextHyp.start - hyp.end
                if hypEndToNextHyp > 1:  # gap
                    # get content within that gap
                    extra = ref.content[len(hyp.content):len(hyp.content) + hypEndToNextHyp]
                    print ref.content[len(hyp.content) - 10:len(hyp.content) + hypEndToNextHyp + 10]
                    print nextHyp.start, hyp.end
                    print "extra::", extra
                    if re.search('[a-zA-Z0-9]', extra):  # TODO: may be add more to this regex\
                        realLeak = True
                        print 'Warning, gap contains real character in ' + self.filename
                if realLeak:
                    tempRef = Annotation(ref.start, hyp.end + len(extra)
                                         , ref.content[0:len(hyp.content) + len(extra)], ref.label)
                    self.references.insert(0, tempRef)
                    # self.processFullSpanMatch()
                    self.processOvermarks()
                    ref.start = hyp.end + len(extra)
                    ref.content = ref.content[len(hyp.content) + len(extra):]
                elif hyp.label != nextHyp.label or re.search(' ', hyp.content) or re.search(' ', nextHyp.content):
                    # split up ref and create match/tagclash for hyp'
                    tempRef = Annotation(ref.start, hyp.end, ref.content[0:len(hyp.content)], ref.label)
                    self.references.insert(0, tempRef)
                    self.processFullSpanMatch()
                    ref.start = hyp.end + len(extra)
                    ref.content = ref.content[len(hyp.content) + len(extra):]
                else:
                    # if (0 <= hypEndToNextHyp <= 2): #tolerance of 2 characters
                    hyp.end = nextHyp.end
                    hyp.content += extra + nextHyp.content
                    self.hypotheticals.pop(1)
                    surr = self.getSurrogates()
                    nextSurr = self.getSurrogates()
                    if surr and nextSurr:
                        surr.end = nextSurr.end
                        surr.content += extra + nextSurr.content
                    if surr:
                        self.surrogates.insert(0, surr)
                        # else:
                        #    #split up ref and create match/tagclash for hyp
                        #    tempRef = Annotation(ref.start, hyp.end, ref.content[0:len(hyp.content)], ref.label)
                        #    self.references.insert(0, tempRef)
                        #    self.processFullSpanMatch()
                        #    ref.start = hyp.end
                        #    ref.content = ref.content[len(hyp.content):]

            # update overmarks to check while look condition
            overmarks = ref.findNextOverlap(self.hypotheticals)

    def processUndermarks(self):
        """helper method to process(), combine undermark with missing or keep as undermark"""

        if len(self.references) == 0 or len(self.hypotheticals) == 0:
            raise Exception(
                'Expecting to process an undermark but there is no valid ref or hyp left in ' + self.filename)

        # remove current hyp and see if current hyp span also covers the next ref
        hyp = self.hypotheticals.pop(0)
        ref = self.references[0]

        if not ref.isUndermark(hyp):
            raise Exception(
                'Expecting to process an undermark but ref and hyp is not an undermark pair in ' + self.filename)

        undermarks = hyp.findNextOverlap(self.references)
        while len(undermarks) > 0:
            # resets
            ref = self.references[0]
            extra = ''
            refEndToNextRef = 0

            if len(undermarks) == 1:
                # single undermark
                if ((ref.start == hyp.start and ref.end < hyp.end)
                    or (ref.start > hyp.start and ref.end == hyp.end)
                    or (ref.start > hyp.start and ref.end < hyp.end)):
                    surr = self.getSurrogates()
                    detail = MATExperimentDetail(self.filename)
                    gold = surr
                    resultType = 'undermark'
                    if ref.label != hyp.label: resultType += ',tagclash'
                    self.addToGoldDetails(detail, ref, hyp, surr, gold, resultType)
                    self.references.pop(0)
                elif ref.start == hyp.start and ref.end == hyp.end:
                    self.hypotheticals.insert(0, hyp)
                    self.processFullSpanMatch()
                else:
                    raise Exception('Unexpected case in ' + self.filename)
            else:
                nextRef = self.references[1]
                if (ref.start < hyp.start or ref.end > hyp.end
                    or nextRef.start < hyp.start or nextRef.end > hyp.end):
                    raise Exception('Unexpected gap in ' + self.filename)

                # get gap between ref end to next ref start
                refEndToNextRef = nextRef.start - ref.end
                if refEndToNextRef > 0:
                    # extra = hyp.content[len(ref.content):len(ref.content)+refEndToNextRef]
                    gapStart = ref.end - hyp.start
                    extra = hyp.content[gapStart: gapStart + refEndToNextRef]
                # next ref ends before or at hyp ends, update ref to covers next ref and remove next ref
                if ref.end < nextRef.end:
                    ref.end = nextRef.end
                    if refEndToNextRef >= 0:
                        ref.content += extra + nextRef.content
                    else:
                        ref.content += nextRef.content[-(nextRef.end - ref.end):]
                self.references.pop(1)


                # update undermarks to check while look condition
            undermarks = hyp.findNextOverlap(self.references)

    def processOverlaps(self):
        """helper method to process(), use to split overlap"""

        if len(self.references) == 0 or len(self.hypotheticals) == 0:
            raise Exception('Expecting to process overlap pair but there is no ref or hyp left in ' + self.filename)
        ref = self.references.pop(0)
        hyp = self.hypotheticals[0]

        if ref.start < hyp.start and ref.end > hyp.start and ref.end < hyp.end:
            # overlap case: save 2 rows with missing/leak in front then undermark'
            overlapLen = hyp.start - ref.start
            newRef = Annotation(ref.start, hyp.start, ref.content[0:overlapLen], ref.label)
            self.references.insert(0, newRef)
            self.processFullLeak()

            newRef = Annotation(hyp.start, ref.end, ref.content[overlapLen:], ref.label)
            self.references.insert(0, newRef)
            self.processUndermarks()

        elif ref.start > hyp.start and ref.start < hyp.end and ref.end > hyp.end:
            # overlap case: save 2 rows with undermark then missing/leak'
            overlapLen = hyp.end - ref.start
            newRef = Annotation(ref.start, hyp.end, ref.content[0:overlapLen], ref.label)
            self.references.insert(0, newRef)
            self.processUndermarks()

            newRef = Annotation(hyp.end, ref.end, ref.content[overlapLen:], ref.label)
            self.references.insert(0, newRef)
            self.processFullLeak()
        else:
            raise Exception('Unexpected case in ' + self.filename)

    def processNonFullLeak(self):
        """helper method to process(), use to decide which experiment detail type 
        that is not 'missing' and route to the right method"""

        if len(self.references) > 0 and len(self.hypotheticals) > 0:
            detail = MATExperimentDetail(self.filename)
            ref = self.references[0]
            hyp = self.hypotheticals[0]
            if ref.isMissing(hyp):
                raise Exception('Not expecting missing type when processing non full leak in file ' + self.filename)
            if ref.start >= hyp.end:
                self.processSpurious()
            elif ref.start == hyp.start and ref.end == hyp.end:
                self.processFullSpanMatch()
            elif ref.isOvermark(hyp):
                self.processOvermarks()
            elif ref.isUndermark(hyp):
                self.processUndermarks()
            else:  # probably overlap
                self.processOverlaps()
        else:
            raise Exception(
                'Expecting to process a non leak experiment row but there is no ref or hyp left in ' + self.filename)

    def getSurrogates(self, index=0):
        """return surrogate at index, or next surrogates or None"""
        if len(self.surrogates) > 0 and index < len(self.surrogates):
            return self.surrogates.pop(index)
        else:
            print 'No surrogates available at requessted index in ' + self.filename

    def addToGoldDetails(self, detail, ref, hyp, surr, gold, resultType, surOverlapLen=0):
        """does this for any type of experiment detail in the gold details list"""
        # verify that gold offsets yield correct gold content
        if gold.content != self.surrogateFileText[gold.start:gold.end]:
            errMsg = "Error: gold content did not match expected text at offsets for " + self.filename
            print errMsg
            print '"{0}"'.format(self.surrogateFileText[gold.start:gold.end])
            print '"{0}"'.format(gold.content)
            if False:
                raise Exception(errMsg)

        hypLen = 0
        surLen = 0
        if hyp: hypLen = len(hyp.content)
        if surr: surLen = len(surr.content)

        # TODO: make sure this is the right surr and hyp to calculate surOverlapLen.
        if resultType != 'missing':
            surOverlapLen = self.getContentOverlapLength(surr.content, hyp.start, hyp.end)

        self.cumulativeOffsetChange += (
            surLen - hypLen - surOverlapLen)  # TODO check to see whether this is a ref variable

        detail.setDetail(ref, hyp, surr, gold, resultType, self.cumulativeOffsetChange)
        self.goldDetails.append(detail)

    def getContentOverlapLength(self, contentText, hypStart, hypEnd):
        """Return the length of the overlap content if there was one, or 0"""

        overlapLen = 0

        # find text of token from tokenList where token end == (hypStart - 1)
        textPreHyp = getTokenTextAtOffset(self.originalTokens, endOffset=(hypStart - 1))

        # find text of token from tokenList where token start == hypEnd
        textPostHyp = getTokenTextAtOffset(self.originalTokens, startOffset=hypEnd)

        if textPreHyp != None:
            textSurHead = contentText[0: len(textPreHyp)]
            if textPreHyp == textSurHead:
                print 'surrogate starts with previous text'
                overlapLen = len(textPreHyp)

        if textPostHyp != None:
            textSurTail = contentText[-len(textPostHyp):]
            if textSurTail == textPostHyp:
                print 'surrogate ends with following text'
                overlapLen += len(textPostHyp)

        return overlapLen

  