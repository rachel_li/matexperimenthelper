################################################################
# DT Tran
# 1/24/2014
# Compare the gold standards across multiple MIST workspaces
# (same corpus)
################################################################
import csv
import collections
from MatJSONDocument import *

fileList = r'\\groups\data\CTRHS\HIPS\Experiments\Tagger_Performance\Exp39\NewFamPracGoldList.txt'
outFile = r'\\groups\data\CTRHS\HIPS\Experiments\Gold_Standard_Validation\Test\gold_compare.csv' #by type for each workspace, not line up
outFile2 = r'\\groups\data\CTRHS\HIPS\Experiments\Gold_Standard_Validation\Test\gold_compare2.csv' #by type,begin,offset, print all, line up
outFile3= r'\\groups\data\CTRHS\HIPS\Experiments\Gold_Standard_Validation\Test\gold_compare3.csv' #by type,begin,offset, print diff only, line up
outFile4= r'\\groups\data\CTRHS\HIPS\Experiments\Gold_Standard_Validation\Test\gold_compare4.csv'
outFile5= r'\\groups\data\CTRHS\HIPS\Experiments\Gold_Standard_Validation\Experiment2Corpus_All.csv'
outFile6= r'\\groups\data\CTRHS\HIPS\Experiments\Gold_Standard_Validation\Experiment2Corpus_DocDiff.csv'
outFile7= r'\\groups\data\CTRHS\HIPS\Experiments\Gold_Standard_Validation\Experiment2Corpus_LineDiff.csv'

fileListRows = []
workspaces = collections.OrderedDict([("ws2", r'\\groups\data\CTRHS\HIPS\WS\ws2')
              ,("ghri1", r'\\groups\data\CTRHS\HIPS\WS\exp2\ghri1')
              ,("ghri2", r'\\groups\data\CTRHS\HIPS\WS\exp2\ghri2')
              ,("ghri3", r'\\groups\data\CTRHS\HIPS\WS\exp2\ghri3')
              ,("ghri4", r'\\groups\data\CTRHS\HIPS\WS\exp2\ghri4')
              ])
#goldStandards = {}
goldStandards = collections.OrderedDict([])
goldsToCompare = []

types = ['address','age','date','doc_initials','doc_name','email','ip_addr'
         ,'med_rec_num','org_name','other_id'
        ,'phone','pt_name','room','ssn','url'
        ]

def main():
    """does not line up the PII instances"""

    if fileList != '':
            with open(fileList, 'rb') as fL:
                fileListRows = [elem for elem in fL.read().split() if elem]
    contents = []
    for f, filename in enumerate(fileListRows):
        goldsToCompare = []
        count = 0
        maxCount = 0
        for i, ws in workspaces.iteritems():
            doc = MatJSONDocument(ws + '\\folders\core\\' + filename)
            piiList = doc.getAsetTypeList(types)
            count = len(piiList)
            if (count > maxCount):
                maxCount = count

            goldsToCompare.append(piiList)
            

        for annot in range(maxCount):
            #TODO: extractGold with any number of given workspaces
            ws0 = extractGold(goldsToCompare[0], annot)
            ws1 = extractGold(goldsToCompare[1], annot)
            ws2 = extractGold(goldsToCompare[2], annot)
            ws3 = extractGold(goldsToCompare[3], annot)
            ws4 = extractGold(goldsToCompare[4], annot)
            currRow = [filename, ws0['type'], ws0['begin'], ws0['end']
                        , ws1['type'], ws1['begin'], ws1['end']
                        , ws2['type'], ws2['begin'], ws2['end']
                        , ws3['type'], ws3['begin'], ws3['end']
                        , ws4['type'], ws4['begin'], ws4['end'],'', '' #TODO add PII and context
                        ]
            contents.append(currRow)

    writeOutCompare(outFile, contents)

def main2(printToFile, byBeginOffsetsOnly, printOnlyDocDiff, printOnlyLineDiff):
    """line up the pii instances, if printOnlyDiff is true, only print documents with at least one discrepancy"""

    if fileList != '':
            with open(fileList, 'rb') as fL:
                fileListRows = [elem for elem in fL.read().split() if elem]
    contents = []
    for f, filename in enumerate(fileListRows):
        #get the gold standards in each workspace for each file
        goldStandards = collections.OrderedDict([])
        goldsToCompare = []
        for i, ws in workspaces.iteritems():
            doc = MatJSONDocument(ws + '\\folders\core\\' + filename)
            piiList = doc.getAsetTypeList(types)
            piiList.sort()
            for p in piiList:
                type = p['type']
                begin = p['begin']
                end = p['end']
                if (byBeginOffsetsOnly):
                    key = str(begin) + ','+ str(end)
                    if key not in goldStandards:
                        goldStandards[key] = {i:type}
                    else:
                        goldStandards[key].update({i:type})
                else:
                    key = type + ','+ str(begin) + ','+ str(end)
                    #record which workspace also annotated this pii
                    #all pii are recorded into the goldStandards object
                    #, even if just 1 workspace annotated it
                    if key not in goldStandards:
                        goldStandards[key] = [i]
                    else:
                        goldStandards[key].append(i)
        
        #when done collecting all pii in all workspaces
        #iterate through the list and line up those instances
        #if all workspace has the same pii, then write out the PII and the context around that PII

        #start by assuming that all workspace agree for this document
        docDiff = False
        goldsToCompare = sorted(goldStandards, cmp = compareOffsetsKey)
        #for g, wsList in goldStandards.iteritems():
        for g in goldsToCompare:
            wsList = goldStandards[g]
            currRow =[filename]
            lineDiff = False

            if(byBeginOffsetsOnly):
                begin,end = g.split(',')
                for i in workspaces:
                    if i in wsList:
                       currRow.extend([wsList[i], begin, end])
                    else:
                        currRow.extend([None,None,None])
                        lineDiff = True
                        docDiff = True
            else:
                type, begin,end = g.split(',')
                for i in workspaces:
                    if i in wsList:
                        currRow.extend([type, begin, end])
                    else:
                        currRow.extend([None,None,None])
                        lineDiff = True
                        docDiff = True
            
            #if lineDiff:
            #    currRow.extend(['',''])
            #else:
            currRow.extend([doc.text[int(begin):int(end)]
                                , doc.text[max(0,int(begin)-20): min(int(end)+20, len(doc.text))]])
            
            if printOnlyLineDiff:
                if lineDiff:
                    contents.append(currRow)
            else:
                contents.append(currRow)

        # if docDif never set to True across all workspaces for this document
        # then redact it out of the contents to be printed
        if (len(goldStandards) == 0):
            print filename+ ' has 0 pii in any workspace'
        elif (printOnlyDocDiff and (not docDiff)):# and len(goldStandards)>0):
            del contents[-len(goldStandards):]
            print filename +' has no disagreement in all pii counts: ' + str(len(goldStandards))

    writeOutCompare(printToFile, contents)

def writeOutCompare(outFile, compareContents):

    csv.register_dialect('escapedtab', escapechar = ",",quotechar="'"
                         ,doublequote = True, quoting=csv.QUOTE_ALL
                         , delimiter ='\t')
    csv.register_dialect('escapedcomma', escapechar = '"',quotechar='"'
                         ,doublequote = True, quoting=csv.QUOTE_ALL
                         , delimiter =',')
    csv.register_dialect('singlequote',quotechar="'", doublequote = True
                         , quoting = csv.QUOTE_ALL)

    
    try:
        #create a csv file with all pii instances annotated by each workspace
        with open(outFile, 'wb') as fileOut:
            writer = csv.writer(fileOut,  )
            writer.writerow(['file','gs0_type', 'gs0_begin', 'gs0_end'
                                    ,'gs1_type', 'gs1_begin', 'gs1_end'
                                    ,'gs2_type', 'gs2_begin', 'gs2_end'
                                    ,'gs3_type', 'gs3_begin', 'gs3_end'
                                    ,'gs4_type', 'gs4_begin', 'gs4_end','PII', 'Context']
                                    )
            for row in compareContents:
                writer.writerow(row)

    except Exception as e:
        print "Error: fail to write out a PII instance to the gold_compares.csv"
    
def compareOffsetsKey(key1,key2):
    beg1,end1 = key1.split(',')
    beg2,end2 = key2.split(',')
    if int(beg1)<int(beg2):
        return -1
    elif int(beg1) > int(beg2):
        return 1
    else:
        return 0

def extractGold(gold, atIndex):
    try:
        return gold[atIndex]
    except:
        return {'type':'', 'begin': None, 'end': None}
if __name__ == "__main__":
    main2(outFile7, True, True,True)