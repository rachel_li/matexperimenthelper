import unittest

from MatJSONReader import *
from MatJSONDocument import *

class TestMATExperimentHelper(unittest.TestCase):
    #def setUp(self):
    #def tearDown(self):

    def test_MatJSONDocument(self):

        matjsonFile = r'\\groups\data\CTRHS\HIPS\Experiments\Human\Test\Documents\Surrogatized\MatJson\3817.prepped.tag.json'
        rawFile = r'\\groups\data\CTRHS\HIPS\Experiments\Human\Test\Documents\Surrogatized\RawFormat\3817.prepped.tag.json'
        getContentText(matjsonFile)

        doc = MatJSONDocument(matjsonFile)

        #should return a dictionary, where label is the key and the value is an array of array
        asets = doc.getAsetType(['org_name'])
        self.assertIs(type(asets), dict)
        self.assertIs(type(asets['org_name']), list)

        asets = doc.getAsetType(['org_name', 'age'])
        self.assertIs(type(asets), dict)
        self.assertIs(type(asets['age']), list)

        #count 1 type
        asetsCount = doc.getAsetTypeCount(['org_name'])
        self.assertEqual(asetsCount, {'org_name': 4})

        #count > 1 type
        asetsCount = doc.getAsetTypeCount(['org_name', 'age'])
        self.assertEqual(asetsCount, {'org_name': 4, 'age': 2})

        #when type doesn't exist
        asets = doc.getAsetType(['email'])
        self.assertIs(type(asets), dict)
        self.assertEqual(asets, {})

        asetsCount = doc.getAsetTypeCount(['email'])
        self.assertEqual(asetsCount, {'email': 0})

        #when testFile is not in mat-json format, should raise an error
        with self.assertRaises(ValueError):
            MatJSONDocument(rawFile)

        #TODO: when signal/text portion of the file contains special character or quote, it should format properly


if __name__ == '__main__':
    unittest.main()